#!/bin/sh -eux

cd "$(dirname -- "$0")"

# 1. compile
cd packages
for dir in $(find -mindepth 1 -maxdepth 1 -type d); do
	cd $dir
	gbp buildpackage --git-pbuilder --git-ignore-branch
	cd ..
done
cd ..

# 2. build repo and serve
mkdir -p repo
./update-repo.sh ./packages ./repo

# 3
python3 -m http.server 8888 -d ./repo &
httpd_pid=$!

# 3. bootstrap

debvm-create -o usr-test.ext4 --release unstable -- \
	--variant apt "deb http://deb.debian.org/debian unstable main " "deb [trusted=yes] http://localhost:8888 UNRELEASED main"
debvm-run -i usr-test.ext4

# 4, cleanup
kill $httpd_pid
