# What breaks if we move files from `/{bin,lib}` to `/usr`?

This repo contains a scrappy script that bootstraps an unstable VM
using packages that have modified to install their files in `/usr`.

Dependencies: `debvm`, `mmdebstrap`, `gbp`, common development tools.

To test:

1. checkout all submodules in `packages/` (`git submodule init && git submodule update`);
2. run `run.sh`;
3. see if anything broke.

