#!/bin/bash

set -eu
set -o pipefail

incoming_dir="$1"
output_repo="$2"

export LC_ALL=C.UTF-8
NOW=$(date --rfc-email --utc)
component=main

if [ -e "$2" ] ; then
	echo "ERROR: $2 already exists"
	#exit 1
fi

changes_files=$(find "$incoming_dir" -name "*_amd64.changes" | sort)
for changes_file in $changes_files ; do
	distro=$(cat "$changes_file" | grep "^Distribution: " | cut -d' ' -f2)
	component_dir="$output_repo/dists/$distro/$component/binary-amd64"
	mkdir -p "$component_dir"

	deb_files=$(cat "$changes_file" | grep -E '^ [a-f0-9]{32} ' | grep '\.deb$' | grep -v 'dbgsym_' | cut -d ' ' -f6)
	for deb_file in $deb_files ; do
		cp -a "$incoming_dir/$deb_file" "$component_dir"
	done
done

#deb_files=$(find "$incoming_dir" -name "*_a*.deb" | sort)
#for deb_file in $deb_files ; do
#	changes_file="${deb_file%_*.deb}_amd64.changes"
#
#	# TODO: if ! changes_file distro=unstable
#	distro=$(cat $changes_file | grep "^Distribution: " | cut -d' ' -f2)
#
#	component_dir="$output_repo/dists/$distro/$component/binary-amd64"
#	mkdir -p $component_dir
#
#	echo cp -a $deb_file $component_dir
#done

cd "$output_repo"
distros=$(find dists/ -name "*_a*.deb" | cut -d/ -f2 | sort | uniq)
for distro in $distros ; do
	echo "$distro"

	packages_file="dists/$distro/$component/binary-amd64/Packages"
	release_file="dists/$distro/Release"
	inrelease_file="dists/$distro/InRelease"

	dpkg-scanpackages --multiversion dists/$distro > $packages_file
	gzip -9 --force --keep $packages_file

	# TODO Version:
	cat > $release_file <<-EOF
		Origin: XXXX Origin
		Label: XXXX Label
		Suite: $distro
		Codename: $distro
		Date: $NOW
		Architectures: amd64
		Components: main
		Acquire-By-Hash: no
		Description: XXXX Description
		SHA256:
	EOF

#	byhash_dir="dists/$distro/by-hash/SHA256"
#	mkdir -p "$byhash_dir"
	for f in Packages Packages.gz ; do
		ff="dists/$distro/$component/binary-amd64/$f"
		h=$(sha256sum "$ff" | cut -d' ' -f1)
		size=$(du -b "$ff" | cut -f1)
		printf " %s %15s %s\n" "$h" "$size" "$component/binary-amd64/$f" >> $release_file
#
#		cp -a $ff "$byhash_dir/$h"
	done

	gpg --clearsign -o - $release_file > $inrelease_file
	gpg --detach-sign --armor --batch --yes -o $release_file.gpg $release_file
done
